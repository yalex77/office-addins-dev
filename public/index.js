// Office.onReady();
Office.initialize = function (reason) {};

function onNewMessageComposeHandler(event) {
  const userEmail = Office.context.mailbox.userProfile.emailAddress;
  const signatureUrl = 'https://newoldstamp.com/offices/signatures/' + userEmail;

  fetch(signatureUrl)
    .then(res => res.text())
    .then(signature => {
      if (signature.length) {
        Office.context.mailbox.item.body.setSignatureAsync(
          signature,
          {
            coercionType: "html",
            asyncContext: event,
          },
          addSignatureCallback
        );
      }
    }
  );
}

// The OnMessageFromChanged event handler that updates the signature when the email address in the From field is changed.
function onMessageFromChangedHandler(event) {
    const item = Office.context.mailbox.item;

    // Get the currently selected From account.
    item.from.getAsync({ asyncContext: event }, (result) => {
        if (result.status === Office.AsyncResultStatus.Failed) {
            console.log(result.error.message);
            return;
        }

        // Create a signature based on the currently selected From account.
        const name = result.value.emailAddress;
        const signatureUrl = 'https://newoldstamp.com/offices/signatures/' + name;
             fetch(signatureUrl)
            .then(res => res.text())
            .then(signature => {
              if (signature.length) {
                Office.context.mailbox.item.body.setSignatureAsync(
                 signature,
                 {
                   coercionType: "html",
                   asyncContext: event,
                 },
          addSignatureCallback
        );
      }
    }
       );




    });
}

// Callback function to add a signature to the mail item.
function addSignatureCallback(result) {
    if (result.status === Office.AsyncResultStatus.Failed) {
        console.log(result.error.message);
        return;
    }

    console.log("Successfully added signature.");
    result.asyncContext.completed();
}


// IMPORTANT: To ensure your add-in is supported in the Outlook client on Windows, remember to
// map the event handler name specified in the manifest's LaunchEvent element to its JavaScript counterpart.
// if (Office.context.platform === Office.PlatformType.PC || Office.context.platform === null) {
Office.actions.associate("onNewMessageComposeHandler", onNewMessageComposeHandler);
Office.actions.associate("onMessageFromChangedHandler", onMessageFromChangedHandler);
// }
